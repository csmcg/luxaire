#!/usr/bin/python3
import pandas as pd
import numpy as np
import pdb
import re
import sys

if len(sys.argv) is not 3:
    print('Syntax: ./script-name.py infile.xlsx outfile.xlsx')
    sys.exit()

input = pd.read_excel(sys.argv[1], index_col = None, sheet_name=0) # input file, AHRI data. Selects first sheet in workbook
output = pd.DataFrame() # the eventualy outfile, PBD upload format
output = pd.DataFrame(columns=input.columns.values.tolist()) # give output file the same header as input file
coil_ref = pd.read_excel('coil-ref.xlsx', index_col=0, header=None) # tabular reference sheet that uses the important 
                                                                                  # info of AHRI nomenclature (CF/CM*48B*+TXV) and matches that with coils existing in the database
txv_ref = pd.read_excel('txv-ref.xlsx', index_col=0, header=None) # reference file: outdoor unit -> TXV kit

# ALGORITHM

# 1. open input
# 2. for each matchup:
#   2a. read coil capacity and width (3 chars before '+')
#   2b. grab from coil_ref list of coils that match the coil spec
#   2c. if coil has X in it, look up model 1 (hp) in txv-ref for list of txv
#       2c-i. for each txv record also add Champion to 'brand 4'
#   2c. copy matchup and replace ahri coil with one from list 
# 3. save


for index, matchup in input.iterrows(): # for each line in the input file
	matchups_to_add = pd.DataFrame(columns=input.columns.values.tolist()) # dataframe of matches for this row. matchups that fit the AHRI spec are added into this dataframe to eventually be added to output file

	#pdb.set_trace()

	# SPLIT GAS ELECTRIC
	if matchup['system_type_id'] == 1 or matchup['system_type_id'] == 5:
		ahri_coil = matchup['model 3'] # get the AHRI nomenclature of the coil

		# Figure out the coil specification
		# leave alone the CM+TXV&EEV
		if re.search('E', ahri_coil) is not None: # found an E, meaning it's a eev
			output = output.append(matchup) # add it to output file
			continue # continue iteration, skipping the rest of the loop
		elif re.search('/', ahri_coil) is None:
			continue

		#coil_spec = re.search('...\\+', ahri_coil) # use this for regular +TXV matches
		coil_spec = re.search('.{3}$', ahri_coil) # use this if going for no txv matches
		
		if coil_spec is not None:
			coil_spec = coil_spec.group(0).rstrip('+') # search the AHRI coil for the 
														 # 3 chars behind +TXV, the 
														 # cap. (XX) and width (A/B/C/D)
		else:
			continue
		### pretty sure this is totally useless
		#if coil_spec is None: # if no match, skip
		#    pass
	   
		coil_matches = coil_ref.loc[coil_spec]

		for coil in coil_matches:
			if coil is np.nan:
				break
			#pdb.set_trace()
			new_mup = matchup.copy()
			new_mup['model 3'] = coil
			if re.search('X', coil) is not None: # needs a txv if X in model number
				if re.search('THE', new_mup['model 1']) is None: # if not THE outdoor
					hp = re.search('.*B..', new_mup['model 1']).group(0)
					txv_matches = txv_ref.loc[hp]
					for txv in txv_matches:
						if txv is not np.nan:
							txv_mup = new_mup.copy()
							txv_mup['model 4'] = txv
							txv_mup['brand 4'] = 'Luxaire'
							matchups_to_add = matchups_to_add.append(txv_mup)
				else:
					hp = re.search('THE..', new_mup['model 1']).group(0)
					txv_matches = txv_ref.loc[hp]
					for txv in txv_matches:
						if txv is not np.nan:
							txv_mup = new_mup.copy()
							txv_mup['model 4'] = txv
							txv_mup['brand 4'] = 'Luxaire'
							matchups_to_add = matchups_to_add.append(txv_mup)

			else:
				matchups_to_add = matchups_to_add.append(new_mup)

	# SPLIT HEAT PUMP
	elif matchup['system_type_id'] == 3 or matchup['system_type_id'] == 14:
		idu = matchup['model 2'] # the indoor unit ahri nomenclature

		# IF ME OR MVC
		if re.search('ME', idu) or re.search('MVC', idu) is not None: # either ME or MVC indoor unit
			# todo
			# MVC (possibility of no TXV)
			if re.search('MVC', idu) is not None: 
				# check for txv required
				if re.search('\+CC', idu) is not None: # does not require TXV or coil built, since it uses EEV
					ahri_coil = re.search('[^+]*', idu).group(0) # grab the coil
					ah = re.search('(?<=\+)(.*?)(?=\+)', idu).group(0) # returns the MVC ah
					new_mup = matchup.copy()
					new_mup['model 2'] = ah
					new_mup['model 3'] = ahri_coil # add the EEV coil to model 3
					new_mup['brand 3'] = 'Luxaire'
					matchups_to_add = matchups_to_add.append(new_mup)
				else: # need to build the field installed txv coils
					ahri_coil = re.search('[^+]*', idu).group(0) # grab the coil
					coil_spec = ahri_coil[-3:] # grab last 3, XXY, XX = cap and Y = width
					coil_matches = coil_ref.loc[coil_spec]
					ah = re.search('(?<=\+)(.*?)(?=\+)', idu).group(0) # returns ah
					for coil in coil_matches:
						if coil is np.nan:
							break
						new_mup = matchup.copy()
						new_mup['model 2'] = ah
						new_mup['model 3'] = coil
						new_mup['brand 3'] = 'Luxaire'
						# add txvs
						if re.search('THE', new_mup['model 1']) is not None: # if THE hp
							hp = re.search('.....', new_mup['model 1']).group(0) # return THE hp key
						else:
							hp = re.search('.*B..', new_mup['model 1']).group(0)
						txv_matches = txv_ref.loc[hp]
						for txv in txv_matches:
							if txv is not np.nan:
								txv_mup = new_mup.copy()
								txv_mup['model 4'] = txv
								txv_mup['brand 4'] = 'Luxaire'
								matchups_to_add = matchups_to_add.append(txv_mup)
			elif re.search('ME', idu) is not None:
				ahri_coil = re.search('[^+]*', idu).group(0) # grab the coil
				ah = re.search('(?<=\+)(.*?)(?=\+)', idu).group(0) # returns the MVC ah
				new_mup = matchup.copy()
				coil_spec = ahri_coil[-3:] # grab last 3, XXY, XX = cap and Y = width
				coil_matches = coil_ref.loc[coil_spec]

				for coil in coil_matches:
					if coil is np.nan:
						break
					new_mup = matchup.copy()
					new_mup['model 2'] = ah
					new_mup['model 3'] = coil
					new_mup['brand 3'] = 'Luxaire'
					# add txvs
					if re.search('THE', new_mup['model 1']) is not None: # if THE hp
						hp = re.search('.....', new_mup['model 1']).group(0) # return THE hp key
					else:
						hp = re.search('.*B..', new_mup['model 1']).group(0)
						txv_matches = txv_ref.loc[hp]
						for txv in txv_matches:
							if txv is not np.nan:
								txv_mup = new_mup.copy()
								txv_mup['model 4'] = txv
								txv_mup['brand 4'] = 'Luxaire'
								matchups_to_add = matchups_to_add.append(txv_mup)

		
		# IF AVV
		elif re.search('TXV', idu) is None: # doesn't need a TXV, thus is AVV ah			
			ah = idu.replace('+CC','')
			new_mup = matchup.copy()
			new_mup['model 2'] = ah
			matchups_to_add = matchups_to_add.append(new_mup)
		
		# IF OTHER
		else: # otherwise it is just an air handler (AP/AE/AVC/etc.) that requires only a TXV
			ah = idu.replace('+TXV','')
			new_mup = matchup.copy()
			new_mup['model 2'] = ah
			
			if re.search('THE', new_mup['model 1']) is not None: # THE air handler, diff nomenclature than others
				hp = new_mup['model 1'][0:5] # results in "THEXX" where XX = tonnage
				txv_matches = txv_ref.loc[hp]
				
				for txv in txv_matches:
					if txv is not np.nan:
						txv_mup = new_mup.copy()
						txv_mup['model 3'] = txv
						txv_mup['brand 3'] = 'Luxaire'
						matchups_to_add = matchups_to_add.append(txv_mup)

			
			else: # typical nomenclature with XBYY, where x = model group y = tonnage
				hp = re.search('.*B..', new_mup['model 1']).group(0)
				txv_matches = txv_ref.loc[hp]
				for txv in txv_matches:
					if txv is not np.nan:
						txv_mup = new_mup.copy()
						txv_mup['model 3'] = txv
						txv_mup['brand 3'] = 'Luxaire'
						matchups_to_add = matchups_to_add.append(txv_mup)


			
	output = output.append(matchups_to_add)

output.to_excel(sys.argv[2],index=False)
